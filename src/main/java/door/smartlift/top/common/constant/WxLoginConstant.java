package door.smartlift.top.common.constant;

/**
 *微信登陆常量
 * @author Admin
 * @date 2018/3/29
 */
public class WxLoginConstant {

    public static final String DOMAIN_API="api.weixin.qq.com";

    public static final String GRANT_TYPE="authorization_code";

    public static final String Login_URL_SUFFIX="/sns/jscode2session";

    public static final long SESSION_ACTIVE_TIME=1*60*60*1000;

    public static final String RESULT_CODE_KEY="errcode";

    public static final String RESULT_MSG_KEY="errmsg";

    public static final String OPEN_ID_KEY="openid";

    public static final String SESSION_KEY="session_key";

}
