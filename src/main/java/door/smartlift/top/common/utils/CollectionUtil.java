package door.smartlift.top.common.utils;

import java.util.Collection;

/**
 *
 * @author Admin
 * @date 2018/3/29
 */
public class CollectionUtil {

    public static Boolean isEmpty(Collection c){

        if (c ==null){

            return true;
        }

        if (c.size()==0){

            return true;
        }

        return false;
    }

    public static Boolean isNotEmpty(Collection c){

        return ! isEmpty(c);
    }
}
