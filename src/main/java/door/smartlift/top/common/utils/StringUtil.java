package door.smartlift.top.common.utils;

import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Admin
 * @date 2018/3/29
 */
public class StringUtil {

    public static Boolean isEmpty(CharSequence cs){

        return StringUtils.isEmpty(cs);
    }

    public static Boolean isNotEmpty(CharSequence cs){

        return ! isEmpty(cs);
    }
}
