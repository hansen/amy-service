package door.smartlift.top.common.utils;

/**
 *
 * @author Admin
 * @date 2018/4/2
 */
public class WxSession {

    private static ThreadLocal<String> session=new ThreadLocal<>();

    public static void setSessionId(String sessionId){

        session.set(sessionId);
    }

    public static String getSessionId(){

        return session.get();
    }

    public static void clear(){

        session.remove();
    }
}
