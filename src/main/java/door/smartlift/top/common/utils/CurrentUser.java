package door.smartlift.top.common.utils;

import door.smartlift.top.domain.SysUser;

/**
 *
 * @author Admin
 * @date 2018/4/1
 */
public class CurrentUser {

    private static final ThreadLocal<SysUser> currentUser=new ThreadLocal<>();

    public static void setUser(SysUser user){

        currentUser.set(user);
    }

    public static SysUser getUser(){
         return currentUser.get();
    }

    public static void clear(){

        currentUser.remove();
    }
}
