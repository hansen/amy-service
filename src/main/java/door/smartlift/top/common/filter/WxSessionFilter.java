package door.smartlift.top.common.filter;

import door.smartlift.top.common.utils.StringUtil;
import door.smartlift.top.common.utils.WxSession;
import org.springframework.core.annotation.Order;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 *
 * @author Admin
 * @date 2018/4/2
 */
@Order(1)
@WebFilter(filterName = "wxSessionFilter",urlPatterns = "/wx/*")
public class WxSessionFilter implements Filter{

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest request=(HttpServletRequest)servletRequest;

        String sessionId=request.getHeader("sessionId");

        if (StringUtil.isNotEmpty(sessionId)){

            WxSession.setSessionId(sessionId);
        }

        filterChain.doFilter(servletRequest,servletResponse);

    }

    @Override
    public void destroy() {

    }
}
