package door.smartlift.top.dao;

import door.smartlift.top.domain.WxInfo;
import door.smartlift.top.domain.WxInfoExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * @author Admin
 */
public interface WxInfoMapper {

    int countByExample(WxInfoExample example);


    int deleteByExample(WxInfoExample example);


    int deleteByPrimaryKey(Integer id);


    int insert(WxInfo record);


    int insertSelective(WxInfo record);


    List<WxInfo> selectByExample(WxInfoExample example);


    WxInfo selectByPrimaryKey(Integer id);


    int updateByExampleSelective(@Param("record") WxInfo record, @Param("example") WxInfoExample example);


    int updateByExample(@Param("record") WxInfo record, @Param("example") WxInfoExample example);


    int updateByPrimaryKeySelective(WxInfo record);


    int updateByPrimaryKey(WxInfo record);

    /**
     * 小程序查询
     * @return
     */
    WxInfo selectWxInfo();
}