package door.smartlift.top.dao;

import door.smartlift.top.domain.SysConstant;
import door.smartlift.top.domain.SysConstantExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SysConstantMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_constant
     *
     * @mbggenerated Sun Apr 01 19:29:55 CST 2018
     */
    int countByExample(SysConstantExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_constant
     *
     * @mbggenerated Sun Apr 01 19:29:55 CST 2018
     */
    int deleteByExample(SysConstantExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_constant
     *
     * @mbggenerated Sun Apr 01 19:29:55 CST 2018
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_constant
     *
     * @mbggenerated Sun Apr 01 19:29:55 CST 2018
     */
    int insert(SysConstant record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_constant
     *
     * @mbggenerated Sun Apr 01 19:29:55 CST 2018
     */
    int insertSelective(SysConstant record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_constant
     *
     * @mbggenerated Sun Apr 01 19:29:55 CST 2018
     */
    List<SysConstant> selectByExample(SysConstantExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_constant
     *
     * @mbggenerated Sun Apr 01 19:29:55 CST 2018
     */
    SysConstant selectByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_constant
     *
     * @mbggenerated Sun Apr 01 19:29:55 CST 2018
     */
    int updateByExampleSelective(@Param("record") SysConstant record, @Param("example") SysConstantExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_constant
     *
     * @mbggenerated Sun Apr 01 19:29:55 CST 2018
     */
    int updateByExample(@Param("record") SysConstant record, @Param("example") SysConstantExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_constant
     *
     * @mbggenerated Sun Apr 01 19:29:55 CST 2018
     */
    int updateByPrimaryKeySelective(SysConstant record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_constant
     *
     * @mbggenerated Sun Apr 01 19:29:55 CST 2018
     */
    int updateByPrimaryKey(SysConstant record);

    List<SysConstant> selectAllConstant();
}