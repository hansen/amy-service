package door.smartlift.top.dao;

import door.smartlift.top.domain.WxUser;
import door.smartlift.top.domain.WxUserExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * @author Admin
 */
public interface WxUserMapper {

    int countByExample(WxUserExample example);


    int deleteByExample(WxUserExample example);


    int deleteByPrimaryKey(Integer id);


    int insert(WxUser record);


    int insertSelective(WxUser record);


    List<WxUser> selectByExample(WxUserExample example);


    WxUser selectByPrimaryKey(Integer id);


    int updateByExampleSelective(@Param("record") WxUser record, @Param("example") WxUserExample example);


    int updateByExample(@Param("record") WxUser record, @Param("example") WxUserExample example);


    int updateByPrimaryKeySelective(WxUser record);


    int updateByPrimaryKey(WxUser record);

    WxUser selectWxUserBySession(String value);

    int updateWxUserBySession(WxUser wxUser);

    List<WxUser> selectAllWxUser();
}