package door.smartlift.top.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DtRecordExample {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table dt_record
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    protected String orderByClause;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table dt_record
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    protected boolean distinct;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table dt_record
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    protected List<Criteria> oredCriteria;

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table dt_record
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    public DtRecordExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table dt_record
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table dt_record
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    public String getOrderByClause() {
        return orderByClause;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table dt_record
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table dt_record
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    public boolean isDistinct() {
        return distinct;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table dt_record
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table dt_record
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table dt_record
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table dt_record
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table dt_record
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table dt_record
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table dt_record
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andLoginSnIsNull() {
            addCriterion("login_sn is null");
            return (Criteria) this;
        }

        public Criteria andLoginSnIsNotNull() {
            addCriterion("login_sn is not null");
            return (Criteria) this;
        }

        public Criteria andLoginSnEqualTo(String value) {
            addCriterion("login_sn =", value, "loginSn");
            return (Criteria) this;
        }

        public Criteria andLoginSnNotEqualTo(String value) {
            addCriterion("login_sn <>", value, "loginSn");
            return (Criteria) this;
        }

        public Criteria andLoginSnGreaterThan(String value) {
            addCriterion("login_sn >", value, "loginSn");
            return (Criteria) this;
        }

        public Criteria andLoginSnGreaterThanOrEqualTo(String value) {
            addCriterion("login_sn >=", value, "loginSn");
            return (Criteria) this;
        }

        public Criteria andLoginSnLessThan(String value) {
            addCriterion("login_sn <", value, "loginSn");
            return (Criteria) this;
        }

        public Criteria andLoginSnLessThanOrEqualTo(String value) {
            addCriterion("login_sn <=", value, "loginSn");
            return (Criteria) this;
        }

        public Criteria andLoginSnLike(String value) {
            addCriterion("login_sn like", value, "loginSn");
            return (Criteria) this;
        }

        public Criteria andLoginSnNotLike(String value) {
            addCriterion("login_sn not like", value, "loginSn");
            return (Criteria) this;
        }

        public Criteria andLoginSnIn(List<String> values) {
            addCriterion("login_sn in", values, "loginSn");
            return (Criteria) this;
        }

        public Criteria andLoginSnNotIn(List<String> values) {
            addCriterion("login_sn not in", values, "loginSn");
            return (Criteria) this;
        }

        public Criteria andLoginSnBetween(String value1, String value2) {
            addCriterion("login_sn between", value1, value2, "loginSn");
            return (Criteria) this;
        }

        public Criteria andLoginSnNotBetween(String value1, String value2) {
            addCriterion("login_sn not between", value1, value2, "loginSn");
            return (Criteria) this;
        }

        public Criteria andGameNoIsNull() {
            addCriterion("game_no is null");
            return (Criteria) this;
        }

        public Criteria andGameNoIsNotNull() {
            addCriterion("game_no is not null");
            return (Criteria) this;
        }

        public Criteria andGameNoEqualTo(String value) {
            addCriterion("game_no =", value, "gameNo");
            return (Criteria) this;
        }

        public Criteria andGameNoNotEqualTo(String value) {
            addCriterion("game_no <>", value, "gameNo");
            return (Criteria) this;
        }

        public Criteria andGameNoGreaterThan(String value) {
            addCriterion("game_no >", value, "gameNo");
            return (Criteria) this;
        }

        public Criteria andGameNoGreaterThanOrEqualTo(String value) {
            addCriterion("game_no >=", value, "gameNo");
            return (Criteria) this;
        }

        public Criteria andGameNoLessThan(String value) {
            addCriterion("game_no <", value, "gameNo");
            return (Criteria) this;
        }

        public Criteria andGameNoLessThanOrEqualTo(String value) {
            addCriterion("game_no <=", value, "gameNo");
            return (Criteria) this;
        }

        public Criteria andGameNoLike(String value) {
            addCriterion("game_no like", value, "gameNo");
            return (Criteria) this;
        }

        public Criteria andGameNoNotLike(String value) {
            addCriterion("game_no not like", value, "gameNo");
            return (Criteria) this;
        }

        public Criteria andGameNoIn(List<String> values) {
            addCriterion("game_no in", values, "gameNo");
            return (Criteria) this;
        }

        public Criteria andGameNoNotIn(List<String> values) {
            addCriterion("game_no not in", values, "gameNo");
            return (Criteria) this;
        }

        public Criteria andGameNoBetween(String value1, String value2) {
            addCriterion("game_no between", value1, value2, "gameNo");
            return (Criteria) this;
        }

        public Criteria andGameNoNotBetween(String value1, String value2) {
            addCriterion("game_no not between", value1, value2, "gameNo");
            return (Criteria) this;
        }

        public Criteria andDockNameIsNull() {
            addCriterion("dock_name is null");
            return (Criteria) this;
        }

        public Criteria andDockNameIsNotNull() {
            addCriterion("dock_name is not null");
            return (Criteria) this;
        }

        public Criteria andDockNameEqualTo(String value) {
            addCriterion("dock_name =", value, "dockName");
            return (Criteria) this;
        }

        public Criteria andDockNameNotEqualTo(String value) {
            addCriterion("dock_name <>", value, "dockName");
            return (Criteria) this;
        }

        public Criteria andDockNameGreaterThan(String value) {
            addCriterion("dock_name >", value, "dockName");
            return (Criteria) this;
        }

        public Criteria andDockNameGreaterThanOrEqualTo(String value) {
            addCriterion("dock_name >=", value, "dockName");
            return (Criteria) this;
        }

        public Criteria andDockNameLessThan(String value) {
            addCriterion("dock_name <", value, "dockName");
            return (Criteria) this;
        }

        public Criteria andDockNameLessThanOrEqualTo(String value) {
            addCriterion("dock_name <=", value, "dockName");
            return (Criteria) this;
        }

        public Criteria andDockNameLike(String value) {
            addCriterion("dock_name like", value, "dockName");
            return (Criteria) this;
        }

        public Criteria andDockNameNotLike(String value) {
            addCriterion("dock_name not like", value, "dockName");
            return (Criteria) this;
        }

        public Criteria andDockNameIn(List<String> values) {
            addCriterion("dock_name in", values, "dockName");
            return (Criteria) this;
        }

        public Criteria andDockNameNotIn(List<String> values) {
            addCriterion("dock_name not in", values, "dockName");
            return (Criteria) this;
        }

        public Criteria andDockNameBetween(String value1, String value2) {
            addCriterion("dock_name between", value1, value2, "dockName");
            return (Criteria) this;
        }

        public Criteria andDockNameNotBetween(String value1, String value2) {
            addCriterion("dock_name not between", value1, value2, "dockName");
            return (Criteria) this;
        }

        public Criteria andDockIdIsNull() {
            addCriterion("dock_id is null");
            return (Criteria) this;
        }

        public Criteria andDockIdIsNotNull() {
            addCriterion("dock_id is not null");
            return (Criteria) this;
        }

        public Criteria andDockIdEqualTo(String value) {
            addCriterion("dock_id =", value, "dockId");
            return (Criteria) this;
        }

        public Criteria andDockIdNotEqualTo(String value) {
            addCriterion("dock_id <>", value, "dockId");
            return (Criteria) this;
        }

        public Criteria andDockIdGreaterThan(String value) {
            addCriterion("dock_id >", value, "dockId");
            return (Criteria) this;
        }

        public Criteria andDockIdGreaterThanOrEqualTo(String value) {
            addCriterion("dock_id >=", value, "dockId");
            return (Criteria) this;
        }

        public Criteria andDockIdLessThan(String value) {
            addCriterion("dock_id <", value, "dockId");
            return (Criteria) this;
        }

        public Criteria andDockIdLessThanOrEqualTo(String value) {
            addCriterion("dock_id <=", value, "dockId");
            return (Criteria) this;
        }

        public Criteria andDockIdLike(String value) {
            addCriterion("dock_id like", value, "dockId");
            return (Criteria) this;
        }

        public Criteria andDockIdNotLike(String value) {
            addCriterion("dock_id not like", value, "dockId");
            return (Criteria) this;
        }

        public Criteria andDockIdIn(List<String> values) {
            addCriterion("dock_id in", values, "dockId");
            return (Criteria) this;
        }

        public Criteria andDockIdNotIn(List<String> values) {
            addCriterion("dock_id not in", values, "dockId");
            return (Criteria) this;
        }

        public Criteria andDockIdBetween(String value1, String value2) {
            addCriterion("dock_id between", value1, value2, "dockId");
            return (Criteria) this;
        }

        public Criteria andDockIdNotBetween(String value1, String value2) {
            addCriterion("dock_id not between", value1, value2, "dockId");
            return (Criteria) this;
        }

        public Criteria andDockScoreIsNull() {
            addCriterion("dock_score is null");
            return (Criteria) this;
        }

        public Criteria andDockScoreIsNotNull() {
            addCriterion("dock_score is not null");
            return (Criteria) this;
        }

        public Criteria andDockScoreEqualTo(Integer value) {
            addCriterion("dock_score =", value, "dockScore");
            return (Criteria) this;
        }

        public Criteria andDockScoreNotEqualTo(Integer value) {
            addCriterion("dock_score <>", value, "dockScore");
            return (Criteria) this;
        }

        public Criteria andDockScoreGreaterThan(Integer value) {
            addCriterion("dock_score >", value, "dockScore");
            return (Criteria) this;
        }

        public Criteria andDockScoreGreaterThanOrEqualTo(Integer value) {
            addCriterion("dock_score >=", value, "dockScore");
            return (Criteria) this;
        }

        public Criteria andDockScoreLessThan(Integer value) {
            addCriterion("dock_score <", value, "dockScore");
            return (Criteria) this;
        }

        public Criteria andDockScoreLessThanOrEqualTo(Integer value) {
            addCriterion("dock_score <=", value, "dockScore");
            return (Criteria) this;
        }

        public Criteria andDockScoreIn(List<Integer> values) {
            addCriterion("dock_score in", values, "dockScore");
            return (Criteria) this;
        }

        public Criteria andDockScoreNotIn(List<Integer> values) {
            addCriterion("dock_score not in", values, "dockScore");
            return (Criteria) this;
        }

        public Criteria andDockScoreBetween(Integer value1, Integer value2) {
            addCriterion("dock_score between", value1, value2, "dockScore");
            return (Criteria) this;
        }

        public Criteria andDockScoreNotBetween(Integer value1, Integer value2) {
            addCriterion("dock_score not between", value1, value2, "dockScore");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andDurationIsNull() {
            addCriterion("duration is null");
            return (Criteria) this;
        }

        public Criteria andDurationIsNotNull() {
            addCriterion("duration is not null");
            return (Criteria) this;
        }

        public Criteria andDurationEqualTo(Integer value) {
            addCriterion("duration =", value, "duration");
            return (Criteria) this;
        }

        public Criteria andDurationNotEqualTo(Integer value) {
            addCriterion("duration <>", value, "duration");
            return (Criteria) this;
        }

        public Criteria andDurationGreaterThan(Integer value) {
            addCriterion("duration >", value, "duration");
            return (Criteria) this;
        }

        public Criteria andDurationGreaterThanOrEqualTo(Integer value) {
            addCriterion("duration >=", value, "duration");
            return (Criteria) this;
        }

        public Criteria andDurationLessThan(Integer value) {
            addCriterion("duration <", value, "duration");
            return (Criteria) this;
        }

        public Criteria andDurationLessThanOrEqualTo(Integer value) {
            addCriterion("duration <=", value, "duration");
            return (Criteria) this;
        }

        public Criteria andDurationIn(List<Integer> values) {
            addCriterion("duration in", values, "duration");
            return (Criteria) this;
        }

        public Criteria andDurationNotIn(List<Integer> values) {
            addCriterion("duration not in", values, "duration");
            return (Criteria) this;
        }

        public Criteria andDurationBetween(Integer value1, Integer value2) {
            addCriterion("duration between", value1, value2, "duration");
            return (Criteria) this;
        }

        public Criteria andDurationNotBetween(Integer value1, Integer value2) {
            addCriterion("duration not between", value1, value2, "duration");
            return (Criteria) this;
        }
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table dt_record
     *
     * @mbggenerated do_not_delete_during_merge Tue Aug 21 13:47:20 CST 2018
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table dt_record
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}