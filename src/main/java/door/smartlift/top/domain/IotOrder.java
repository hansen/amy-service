package door.smartlift.top.domain;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Admin
 */
@Data
public class IotOrder implements Serializable {

    private Integer id;


    private Integer bid;


    private Integer did;


    private String deviceSn;


    private String orderSn;


    private String deviceType;


    private String status;


    private String wxOrder;


    private String gname;


    private Double gprice;


    private Integer duration;


    private Date createAt;


    private Date updateAt;


    private Integer parentId;


    private String openid;

    private Integer uid;


    private static final long serialVersionUID = 1L;

}