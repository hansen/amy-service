package door.smartlift.top.domain.base;

/**
 * Created by asus on 2016/12/15.
 */
public class BaseDateMembers<K,T> {
    private K k;
    private T t;

    private Double total;

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public BaseDateMembers() {
        super();
    }

    public BaseDateMembers(K k, T t) {
        this.k = k;
        this.t = t;
    }

    public K getK() {

        return k;
    }

    public void setK(K k) {
        this.k = k;
    }

    public T getT() {
        return t;
    }

    public void setT(T t) {
        this.t = t;
    }
}
