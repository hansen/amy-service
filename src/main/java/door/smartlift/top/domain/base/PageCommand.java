package door.smartlift.top.domain.base;

import java.io.Serializable;

/**
 * Created by asus on 2016/12/15.
 */
public class PageCommand implements Serializable {
    public PageCommand() {
        super();
    }

    public PageCommand(int pageIndex, int pageSize) {
        this.pageIndex = pageIndex;
        this.pageSize = pageSize;
    }

    private static final long serialVersionUID = -3018382827530855924L;
    private int pageIndex;
    private int pageSize;
    private Long pageTotal;

    private Integer totalPages;

    public Integer getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }

    public int getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public Long getPageTotal() {
        return pageTotal;
    }

    public void setPageTotal(Long pageTotal) {
        this.pageTotal = pageTotal;
    }
}
