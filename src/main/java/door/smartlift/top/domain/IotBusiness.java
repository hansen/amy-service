package door.smartlift.top.domain;

import lombok.Data;

import java.io.Serializable;

/**
 * @author Admin
 */
@Data
public class IotBusiness implements Serializable {

    private Integer bid;


    private String bname;


    private String person;


    private String phone;


    private Integer account;


    private Double discount;


    private String bank;


    private Integer bankNo;


    private String accountName;


    private String prefix;


    private Long loginTime;


    private Integer parentId;

    private String password;

    private String state;


    private static final long serialVersionUID = 1L;

}