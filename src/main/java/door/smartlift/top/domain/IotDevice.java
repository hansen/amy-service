package door.smartlift.top.domain;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Admin
 */
@Data
public class IotDevice implements Serializable {

    private Integer id;


    private String deviceNum;


    private String deviceSn;

    private Integer bid;


    private String deviceName;


    private String deviceType;


    private Date createAt;


    private Date updateAt;


    private String mobile;


    private String connState;


    private String state;


    private Integer parentId;


    private String bak;

    private String bname;


    private static final long serialVersionUID = 1L;

}