package door.smartlift.top.domain;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Admin
 */
@Data
public class IotGoods implements Serializable {

    private Integer id;


    private Integer bid;


    private String dtype;


    private String gname;


    private String gdesc;


    private Double gprice;


    private Integer gsort;


    private Integer duration;

    private Integer pulse;


    private Date createAt;


    private Date updateAt;

    private String deviceNum;





    private static final long serialVersionUID = 1L;


}