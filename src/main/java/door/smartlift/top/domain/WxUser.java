package door.smartlift.top.domain;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Admin
 */
@Data
public class WxUser implements Serializable {

    private Integer id;


    private String nickName;


    private String realName;


    private String sessionKey;


    private String openId;


    private Long expireDate;

    private Date loginTime;


    private String belongId;

    private String avatarUrl;


    private static final long serialVersionUID = 1L;

}