package door.smartlift.top.domain;

import lombok.Data;

import java.io.Serializable;

/**
 * @author Admin
 */
@Data
public class SysConstant implements Serializable {

    private Integer id;


    private String code;


    private String codeName;


    private String codeValue;


    private String valueBak;


    private static final long serialVersionUID = 1L;

}