package door.smartlift.top.domain;

import java.io.Serializable;
import java.util.Date;

public class DtTeam implements Serializable {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column dt_team.id
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    private Integer id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column dt_team.team_name
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    private String teamName;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column dt_team.team_person
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    private String teamPerson;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column dt_team.login_sn
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    private String loginSn;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column dt_team.game_score
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    private Integer gameScore;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column dt_team.team_bak
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    private String teamBak;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column dt_team.status
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    private Integer status;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column dt_team.start_time
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    private Date startTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column dt_team.end_time
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    private Date endTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column dt_team.durtion
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    private Integer durtion;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table dt_team
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    private static final long serialVersionUID = 1L;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column dt_team.id
     *
     * @return the value of dt_team.id
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column dt_team.id
     *
     * @param id the value for dt_team.id
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column dt_team.team_name
     *
     * @return the value of dt_team.team_name
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    public String getTeamName() {
        return teamName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column dt_team.team_name
     *
     * @param teamName the value for dt_team.team_name
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    public void setTeamName(String teamName) {
        this.teamName = teamName == null ? null : teamName.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column dt_team.team_person
     *
     * @return the value of dt_team.team_person
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    public String getTeamPerson() {
        return teamPerson;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column dt_team.team_person
     *
     * @param teamPerson the value for dt_team.team_person
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    public void setTeamPerson(String teamPerson) {
        this.teamPerson = teamPerson == null ? null : teamPerson.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column dt_team.login_sn
     *
     * @return the value of dt_team.login_sn
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    public String getLoginSn() {
        return loginSn;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column dt_team.login_sn
     *
     * @param loginSn the value for dt_team.login_sn
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    public void setLoginSn(String loginSn) {
        this.loginSn = loginSn == null ? null : loginSn.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column dt_team.game_score
     *
     * @return the value of dt_team.game_score
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    public Integer getGameScore() {
        return gameScore;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column dt_team.game_score
     *
     * @param gameScore the value for dt_team.game_score
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    public void setGameScore(Integer gameScore) {
        this.gameScore = gameScore;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column dt_team.team_bak
     *
     * @return the value of dt_team.team_bak
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    public String getTeamBak() {
        return teamBak;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column dt_team.team_bak
     *
     * @param teamBak the value for dt_team.team_bak
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    public void setTeamBak(String teamBak) {
        this.teamBak = teamBak == null ? null : teamBak.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column dt_team.status
     *
     * @return the value of dt_team.status
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column dt_team.status
     *
     * @param status the value for dt_team.status
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column dt_team.start_time
     *
     * @return the value of dt_team.start_time
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    public Date getStartTime() {
        return startTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column dt_team.start_time
     *
     * @param startTime the value for dt_team.start_time
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column dt_team.end_time
     *
     * @return the value of dt_team.end_time
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    public Date getEndTime() {
        return endTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column dt_team.end_time
     *
     * @param endTime the value for dt_team.end_time
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column dt_team.durtion
     *
     * @return the value of dt_team.durtion
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    public Integer getDurtion() {
        return durtion;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column dt_team.durtion
     *
     * @param durtion the value for dt_team.durtion
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    public void setDurtion(Integer durtion) {
        this.durtion = durtion;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table dt_team
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", teamName=").append(teamName);
        sb.append(", teamPerson=").append(teamPerson);
        sb.append(", loginSn=").append(loginSn);
        sb.append(", gameScore=").append(gameScore);
        sb.append(", teamBak=").append(teamBak);
        sb.append(", status=").append(status);
        sb.append(", startTime=").append(startTime);
        sb.append(", endTime=").append(endTime);
        sb.append(", durtion=").append(durtion);
        sb.append("]");
        return sb.toString();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table dt_team
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        DtTeam other = (DtTeam) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getTeamName() == null ? other.getTeamName() == null : this.getTeamName().equals(other.getTeamName()))
            && (this.getTeamPerson() == null ? other.getTeamPerson() == null : this.getTeamPerson().equals(other.getTeamPerson()))
            && (this.getLoginSn() == null ? other.getLoginSn() == null : this.getLoginSn().equals(other.getLoginSn()))
            && (this.getGameScore() == null ? other.getGameScore() == null : this.getGameScore().equals(other.getGameScore()))
            && (this.getTeamBak() == null ? other.getTeamBak() == null : this.getTeamBak().equals(other.getTeamBak()))
            && (this.getStatus() == null ? other.getStatus() == null : this.getStatus().equals(other.getStatus()))
            && (this.getStartTime() == null ? other.getStartTime() == null : this.getStartTime().equals(other.getStartTime()))
            && (this.getEndTime() == null ? other.getEndTime() == null : this.getEndTime().equals(other.getEndTime()))
            && (this.getDurtion() == null ? other.getDurtion() == null : this.getDurtion().equals(other.getDurtion()));
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table dt_team
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getTeamName() == null) ? 0 : getTeamName().hashCode());
        result = prime * result + ((getTeamPerson() == null) ? 0 : getTeamPerson().hashCode());
        result = prime * result + ((getLoginSn() == null) ? 0 : getLoginSn().hashCode());
        result = prime * result + ((getGameScore() == null) ? 0 : getGameScore().hashCode());
        result = prime * result + ((getTeamBak() == null) ? 0 : getTeamBak().hashCode());
        result = prime * result + ((getStatus() == null) ? 0 : getStatus().hashCode());
        result = prime * result + ((getStartTime() == null) ? 0 : getStartTime().hashCode());
        result = prime * result + ((getEndTime() == null) ? 0 : getEndTime().hashCode());
        result = prime * result + ((getDurtion() == null) ? 0 : getDurtion().hashCode());
        return result;
    }
}