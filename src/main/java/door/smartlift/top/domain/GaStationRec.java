package door.smartlift.top.domain;

import java.io.Serializable;
import java.util.Date;

public class GaStationRec implements Serializable {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column ga_station_rec.id
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    private Integer id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column ga_station_rec.station_no
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    private String stationNo;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column ga_station_rec.front_time
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    private Date frontTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column ga_station_rec.back_time
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    private Date backTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column ga_station_rec.imsi
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    private String imsi;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column ga_station_rec.imei
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    private String imei;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column ga_station_rec.etype
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    private Integer etype;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column ga_station_rec.bak
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    private String bak;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column ga_station_rec.create_at
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    private Date createAt;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table ga_station_rec
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    private static final long serialVersionUID = 1L;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ga_station_rec.id
     *
     * @return the value of ga_station_rec.id
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ga_station_rec.id
     *
     * @param id the value for ga_station_rec.id
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ga_station_rec.station_no
     *
     * @return the value of ga_station_rec.station_no
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    public String getStationNo() {
        return stationNo;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ga_station_rec.station_no
     *
     * @param stationNo the value for ga_station_rec.station_no
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    public void setStationNo(String stationNo) {
        this.stationNo = stationNo == null ? null : stationNo.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ga_station_rec.front_time
     *
     * @return the value of ga_station_rec.front_time
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    public Date getFrontTime() {
        return frontTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ga_station_rec.front_time
     *
     * @param frontTime the value for ga_station_rec.front_time
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    public void setFrontTime(Date frontTime) {
        this.frontTime = frontTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ga_station_rec.back_time
     *
     * @return the value of ga_station_rec.back_time
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    public Date getBackTime() {
        return backTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ga_station_rec.back_time
     *
     * @param backTime the value for ga_station_rec.back_time
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    public void setBackTime(Date backTime) {
        this.backTime = backTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ga_station_rec.imsi
     *
     * @return the value of ga_station_rec.imsi
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    public String getImsi() {
        return imsi;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ga_station_rec.imsi
     *
     * @param imsi the value for ga_station_rec.imsi
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    public void setImsi(String imsi) {
        this.imsi = imsi == null ? null : imsi.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ga_station_rec.imei
     *
     * @return the value of ga_station_rec.imei
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    public String getImei() {
        return imei;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ga_station_rec.imei
     *
     * @param imei the value for ga_station_rec.imei
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    public void setImei(String imei) {
        this.imei = imei == null ? null : imei.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ga_station_rec.etype
     *
     * @return the value of ga_station_rec.etype
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    public Integer getEtype() {
        return etype;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ga_station_rec.etype
     *
     * @param etype the value for ga_station_rec.etype
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    public void setEtype(Integer etype) {
        this.etype = etype;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ga_station_rec.bak
     *
     * @return the value of ga_station_rec.bak
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    public String getBak() {
        return bak;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ga_station_rec.bak
     *
     * @param bak the value for ga_station_rec.bak
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    public void setBak(String bak) {
        this.bak = bak == null ? null : bak.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ga_station_rec.create_at
     *
     * @return the value of ga_station_rec.create_at
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    public Date getCreateAt() {
        return createAt;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ga_station_rec.create_at
     *
     * @param createAt the value for ga_station_rec.create_at
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table ga_station_rec
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", stationNo=").append(stationNo);
        sb.append(", frontTime=").append(frontTime);
        sb.append(", backTime=").append(backTime);
        sb.append(", imsi=").append(imsi);
        sb.append(", imei=").append(imei);
        sb.append(", etype=").append(etype);
        sb.append(", bak=").append(bak);
        sb.append(", createAt=").append(createAt);
        sb.append("]");
        return sb.toString();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table ga_station_rec
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        GaStationRec other = (GaStationRec) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getStationNo() == null ? other.getStationNo() == null : this.getStationNo().equals(other.getStationNo()))
            && (this.getFrontTime() == null ? other.getFrontTime() == null : this.getFrontTime().equals(other.getFrontTime()))
            && (this.getBackTime() == null ? other.getBackTime() == null : this.getBackTime().equals(other.getBackTime()))
            && (this.getImsi() == null ? other.getImsi() == null : this.getImsi().equals(other.getImsi()))
            && (this.getImei() == null ? other.getImei() == null : this.getImei().equals(other.getImei()))
            && (this.getEtype() == null ? other.getEtype() == null : this.getEtype().equals(other.getEtype()))
            && (this.getBak() == null ? other.getBak() == null : this.getBak().equals(other.getBak()))
            && (this.getCreateAt() == null ? other.getCreateAt() == null : this.getCreateAt().equals(other.getCreateAt()));
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table ga_station_rec
     *
     * @mbggenerated Tue Aug 21 13:47:20 CST 2018
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getStationNo() == null) ? 0 : getStationNo().hashCode());
        result = prime * result + ((getFrontTime() == null) ? 0 : getFrontTime().hashCode());
        result = prime * result + ((getBackTime() == null) ? 0 : getBackTime().hashCode());
        result = prime * result + ((getImsi() == null) ? 0 : getImsi().hashCode());
        result = prime * result + ((getImei() == null) ? 0 : getImei().hashCode());
        result = prime * result + ((getEtype() == null) ? 0 : getEtype().hashCode());
        result = prime * result + ((getBak() == null) ? 0 : getBak().hashCode());
        result = prime * result + ((getCreateAt() == null) ? 0 : getCreateAt().hashCode());
        return result;
    }
}