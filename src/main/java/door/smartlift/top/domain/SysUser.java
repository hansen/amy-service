package door.smartlift.top.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Admin
 */
@Data
public class SysUser implements Serializable {

    private Integer uid;


    private String username;


    private String phone;


    @JsonIgnore
    private String password;


    @JsonIgnore
    private String salt;


    private Integer state;


    private Integer belong;


    private Date createAt;


    private Date updateAt;


    private static final long serialVersionUID = 1L;

}