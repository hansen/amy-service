package door.smartlift.top.domain;

import lombok.Data;

import java.io.Serializable;

/**
 * @author Admin
 */
@Data
public class WxInfo implements Serializable {

    private Integer id;


    private String appId;


    private String secrect;


    private String mchId;


    private String url;


    private static final long serialVersionUID = 1L;

}