package door.smartlift.top.domain.info;

import lombok.Data;

import java.util.Date;

/**
 * Created by Admin on 2018/5/7.
 */
@Data
public class TransactionRecordInfo {

    private String orderCode;

    private String wxOrderCode;

    private String businessName;

    private String deviceType;

    private String deviceNum;

    private Double price;

    private Double discount;

    private Double money;

    private String goodsName;

    private Integer duration;

    private String wxName;

    private Date createAt;

}
