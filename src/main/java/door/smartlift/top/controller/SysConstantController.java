package door.smartlift.top.controller;

import door.smartlift.top.domain.IotBusiness;
import door.smartlift.top.domain.SysConstant;
import door.smartlift.top.domain.base.BaseDateMembers;
import door.smartlift.top.domain.base.PageCommand;
import door.smartlift.top.resultBean.ResultBean;
import door.smartlift.top.service.SysConstantService;
import door.smartlift.top.service.WxUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 *
 * @author Admin
 * @date 2018/4/1
 */
@Slf4j
@RestController
@RequestMapping("sys")
public class SysConstantController {

    @Autowired
    private SysConstantService sysConstantService;

    @Autowired
    private WxUserService wxUserService;

    @RequestMapping(value = "/constant", method = RequestMethod.GET, headers = "Accept=application/json")
    public ResultBean<List<SysConstant>> constant(@RequestParam(defaultValue = "DEIVICE_TYPE",required = false) String code){


        List<SysConstant> data=this.sysConstantService.selectByConstantCode(code);

        return new ResultBean<>(data);

    }

    @RequestMapping(value = "wxUser/{pageIndex}/{pageSize}", method = RequestMethod.GET, headers = "Accept=application/json")
    public ResultBean constant(@PathVariable Integer pageIndex,@PathVariable Integer pageSize){


        BaseDateMembers data=this.wxUserService.selectWxUserList(new PageCommand(pageIndex,pageSize));

        return new ResultBean(data);

    }
}
