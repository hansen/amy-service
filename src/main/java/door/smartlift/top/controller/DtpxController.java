package door.smartlift.top.controller;

import door.smartlift.top.domain.*;
import door.smartlift.top.resultBean.ResultBean;
import door.smartlift.top.service.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Admin
 * @date 2018/4/1
 */
@Slf4j
@RestController
@RequestMapping("sys/dtpx")
public class DtpxController {


    @Autowired
    private WxUserService wxUserService;

    @Autowired
    private DtTeamService dtTeamService;



    /**
     * 获取所有团队信息
     * @return
     */
    @RequestMapping(value = "getTeams", method = RequestMethod.GET, headers = "Accept=application/json")
       public ResultBean<Map> teams() {

        log.info("===in  time===");
        List<DtTeam> list = this.dtTeamService.getTeamOrderLogin();
        log.info("===out  time===");

        Map data=new HashMap();
        data.put("list", list);
        return new ResultBean<>(data);
    }


    @RequestMapping(value = "getTeamsByScore", method = RequestMethod.GET, headers = "Accept=application/json")
    public ResultBean<Map> getTeamsByScore() {

        log.info("===in  time===");
        List<DtTeam> list = this.dtTeamService.getTeamOrderScore();
        log.info("===out  time===");

        Map data=new HashMap();
        data.put("list", list);
        return new ResultBean<>(data);
    }

    @RequestMapping(value = "getTeamsByDuration", method = RequestMethod.GET, headers = "Accept=application/json")
    public ResultBean<Map> getTeamsByDuration() {

        log.info("===in  time===");
        List<DtTeam> list = this.dtTeamService.getTeamOrderDuration();
        log.info("===out  time===");

        Map data=new HashMap();
        data.put("list", list);
        return new ResultBean<>(data);
    }


    @RequestMapping(value = "addTeam", method = RequestMethod.POST, headers = "Accept=application/json")
         public ResultBean addTeam(@RequestBody DtTeam dtTeam) throws Exception {

        log.info("===in  addTeam===");
        int result= this.dtTeamService.addDtTeam(dtTeam);

        ResultBean rb = new ResultBean();

        rb.setCode(0);
        rb.setMessage("ok");
        return rb;

//        org.springframework.dao.DuplicateKeyException:

//        {"teamName":"asdfasfd","teamPerson":"韩剑","loginSn":"1001","gameScore":-1,"teamBak":null,"status":null,"startTime":null,"endTime":null}

    }


    @RequestMapping(value = "modifyTeam", method = RequestMethod.POST, headers = "Accept=application/json")
       public ResultBean modifyTeam(@RequestBody DtTeam dtTeam) throws Exception {

        log.info("===in  modifyTeam===");
        int result= this.dtTeamService.updateTeam(dtTeam);

        ResultBean rb = new ResultBean();

        if(result>0){
            rb.setCode(0);
            rb.setMessage("ok");
        }else{
            rb.setCode(1);
            rb.setMessage("update failed");
        }

        return rb;

//        org.springframework.dao.DuplicateKeyException:

//       {"id":4,"teamName":"asdfasfd","teamPerson":"韩剑6","loginSn":"1006","gameScore":-1,"teamBak":null,"status":null,"startTime":null,"endTime":null}
    }


    @RequestMapping(value = "delTeam", method = RequestMethod.GET, headers = "Accept=application/json")
    public ResultBean delTeam(@RequestParam  Integer id) throws Exception {

        log.info("===in  delTeam===");
        int result= this.dtTeamService.deleteTeam(id);

        ResultBean rb = new ResultBean();

        if(result>0){
            rb.setCode(0);
            rb.setMessage("ok");
        }else{
            rb.setCode(1);
            rb.setMessage("del failed");
        }

        return rb;


//        Content-Type  header
//        org.springframework.dao.DuplicateKeyException:

//       {"id":4,"teamName":"asdfasfd","teamPerson":"韩剑6","loginSn":"1006","gameScore":-1,"teamBak":null,"status":null,"startTime":null,"endTime":null}
    }


    @RequestMapping(value = "getGame", method = RequestMethod.GET, headers = "Accept=application/json")
    public ResultBean<Map> getGame() {
        log.info("===in  getGame===");
        DtApp dtApp = this.dtTeamService.getGame();

        Map data=new HashMap();
        data.put("game",dtApp);
        return new ResultBean(data);
    }

    @RequestMapping(value = "startGame", method = RequestMethod.GET, headers = "Accept=application/json")
    public ResultBean<Map> startGame() {

        log.info("===in  startGame===");
        int res = this.dtTeamService.startGame();
        ResultBean rb = new ResultBean();

        return rb;
    }


    @RequestMapping(value = "endGame", method = RequestMethod.GET, headers = "Accept=application/json")
    public ResultBean<Map> endGame() {
        log.info("===in  startGame===");
        int res = this.dtTeamService.endGame();
        ResultBean rb = new ResultBean();
        return rb;
    }


    @RequestMapping(value = "resetGame", method = RequestMethod.GET, headers = "Accept=application/json")
    public ResultBean<Map> resetGame(@RequestParam  String loginSn) {
        log.info("===in  resetGame===");
        ResultBean rb = this.dtTeamService.resetGame(loginSn);
        return rb;
    }


    @RequestMapping(value = "getGameResult", method = RequestMethod.GET, headers = "Accept=application/json")
    public ResultBean<Map> getGameResult(@RequestParam  String loginSn) {
        log.info("===in  getGameResult===");
        List<DtRecord> list = this.dtTeamService.getGameResult(loginSn);


        Map data=new HashMap();
        data.put("res",list);

        return new ResultBean<>(data);

//        {"message":"success","code":0,"data":{"res":[{"id":1,"loginSn":"1001","gameNo":"1","dockName":"扣分项1","dockId":"a001","dockScore":3,"createTime":null},{"id":2,"loginSn":"1001","gameNo":"1","dockName":"扣分项2","dockId":"a003","dockScore":5,"createTime":null}]}}
    }




}
