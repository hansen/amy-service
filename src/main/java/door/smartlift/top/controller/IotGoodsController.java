package door.smartlift.top.controller;

import door.smartlift.top.domain.IotBusiness;
import door.smartlift.top.domain.IotGoods;
import door.smartlift.top.resultBean.ResultBean;
import door.smartlift.top.service.IotGoodsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 *
 * @author Admin
 * @date 2018/4/1
 */
@Slf4j
@RestController
@RequestMapping("sys/goodsManager")
public class IotGoodsController {

    @Autowired
    private IotGoodsService iotGoodsService;

    @RequestMapping(value = "/new", method = RequestMethod.POST, headers = "Accept=application/json")
    public ResultBean<Integer> addGoods(@RequestBody IotGoods iotGoods){

        log.info("add iotGoods");
        int num=this.iotGoodsService.addGoods(iotGoods);
        return new ResultBean<>(num);

    }


    @RequestMapping(value = "/update", method = RequestMethod.POST, headers = "Accept=application/json")
    public ResultBean<Integer> updateGoods(@RequestBody IotGoods iotGoods){

        int num=this.iotGoodsService.updateGoodsById(iotGoods);
        return new ResultBean<>(num);

    }

    @RequestMapping(value = "/{bid}", method = RequestMethod.GET, headers = "Accept=application/json")
    public ResultBean<Map> businessGoods(@PathVariable Integer bid){


        Map map=this.iotGoodsService.selectAllGoodsByBid(bid);

        return new ResultBean<>(map);

    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET, headers = "Accept=application/json")
    public ResultBean<Integer> updateGoods(Integer goodsId){

        int num=this.iotGoodsService.deleteGoodsById(goodsId);

        return new ResultBean<>(num);

    }



}
