package door.smartlift.top.controller;

import door.smartlift.top.resultBean.ResultBean;
import door.smartlift.top.service.WxLoginService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 *
 * @author Admin
 * @date 2018/3/29
 */
@RestController
@RequestMapping("wx")
@Slf4j
public class WxLoginController{

    @Autowired
    private WxLoginService wxLoginService;

    @RequestMapping(value = "/login", method = RequestMethod.GET, headers = "Accept=application/json")
    public ResultBean<String> login(@RequestParam String code){

        String sessionKey=this.wxLoginService.wxLogin(code);
        return new ResultBean<>(sessionKey);
    }
}
