package door.smartlift.top.controller;

import door.smartlift.top.domain.IotBusiness;
import door.smartlift.top.domain.base.BaseDateMembers;
import door.smartlift.top.domain.base.PageCommand;
import door.smartlift.top.resultBean.ResultBean;
import door.smartlift.top.service.IotOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Admin on 2018/5/7.
 */
@RestController
@RequestMapping("sys/orderManager")
public class IotOrderController {

    @Autowired
    private IotOrderService iotOrderService;

    @RequestMapping(value = "/list/{pageIndex}/{pageSize}", method = RequestMethod.GET, headers = "Accept=application/json")
    public ResultBean transactionRecord(@PathVariable Integer pageIndex, @PathVariable Integer pageSize, @RequestParam(defaultValue = "pay") String payStatus, String deviceNum, String wxOrderCode){


        BaseDateMembers data=this.iotOrderService.transactionRecord(new PageCommand(pageIndex,pageSize),payStatus,deviceNum,wxOrderCode);

        return new ResultBean<>(data);

    }
}
