package door.smartlift.top.controller;

import door.smartlift.top.domain.IotBusiness;
import door.smartlift.top.resultBean.ResultBean;
import door.smartlift.top.service.IotBusinessService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 *
 * @author Admin
 * @date 2018/4/1
 */
@Slf4j
@RestController
@RequestMapping("sys/businessManager")
public class IotBusinessController {

    @Autowired
    private IotBusinessService iotBusinessService;

    @RequestMapping(value = "/new", method = RequestMethod.POST, headers = "Accept=application/json")
    public ResultBean<Integer> addBusiness(@RequestBody IotBusiness iotBusiness){

        log.info("add iotBusiness");
        int num=this.iotBusinessService.createBusiness(iotBusiness);

        return new ResultBean<>(num);

    }

    @RequestMapping(value = "/update", method = RequestMethod.POST, headers = "Accept=application/json")
    public ResultBean<Integer> updateBusiness(@RequestBody IotBusiness iotBusiness){

        log.info("update iotBusiness");
        int num=this.iotBusinessService.updateBusiness(iotBusiness);

        return new ResultBean<>(num);

    }

    @RequestMapping(value = "/list", method = RequestMethod.GET, headers = "Accept=application/json")
    public ResultBean<List<IotBusiness>> AllBusiness(){


        List<IotBusiness> data=this.iotBusinessService.selectAllBusiness();

        return new ResultBean<>(data);

    }





}
