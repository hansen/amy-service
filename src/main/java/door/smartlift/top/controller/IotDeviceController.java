package door.smartlift.top.controller;

import door.smartlift.top.domain.IotBusiness;
import door.smartlift.top.domain.IotDevice;
import door.smartlift.top.resultBean.ResultBean;
import door.smartlift.top.service.IotDeviceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 *
 * @author Admin
 * @date 2018/4/1
 */
@Slf4j
@RestController
@RequestMapping("sys/deviceManager")
public class IotDeviceController {

    @Autowired
    private IotDeviceService iotDeviceService;

    @RequestMapping(value = "/new", method = RequestMethod.POST, headers = "Accept=application/json")
    public ResultBean<Integer> addDevice(@RequestBody IotDevice iotDevice){

        log.info("add iotDevice");
        int num=this.iotDeviceService.createDevice(iotDevice);

        return new ResultBean<>(num);

    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
    public ResultBean oneDevice(@PathVariable Integer id){

        IotDevice device=this.iotDeviceService.selectDeviceById(id);

        return new ResultBean<>(device);

    }



    @RequestMapping(value = "/renew", method = RequestMethod.POST, headers = "Accept=application/json")
    public ResultBean<Integer> updateDevice(@RequestBody IotDevice iotDevice){

        log.info("update iotDevice");
        int num=this.iotDeviceService.updateDevice(iotDevice);

        return new ResultBean<>(num);

    }

    @RequestMapping(value = "/list", method = RequestMethod.GET, headers = "Accept=application/json")
    public ResultBean<List<IotDevice>> deviceList(Integer bid, String deviceType ){

        List<IotDevice> data=this.iotDeviceService.selectDeviceByExample(bid,deviceType);

        return new ResultBean<>(data);

    }

    @RequestMapping(value = "/allote", method = RequestMethod.POST, headers = "Accept=application/json")
    public ResultBean<Integer> businessList(IotDevice device ){

        int num=this.iotDeviceService.alloteDevice(device);

        return new ResultBean<>(num);

    }
}
