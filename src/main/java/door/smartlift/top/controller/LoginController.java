package door.smartlift.top.controller;

import door.smartlift.top.domain.SysUser;
import door.smartlift.top.resultBean.ResultBean;
import door.smartlift.top.service.SysUserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Admin
 * @date 2018/4/1
 */
@Slf4j
@RestController
public class LoginController {

    @Autowired
    private SysUserService sysUserService;

    @RequestMapping(value = "sys/login",method = RequestMethod.GET,headers = "Accept=application/json")
    public ResultBean<String> showLoginForm(HttpServletRequest req, HttpServletResponse response){

        Subject currentUser = SecurityUtils.getSubject();

        ResultBean<String> resultBean=new ResultBean<>();
        if (!currentUser.isAuthenticated()) {
            resultBean.setMessage("请先登录");
            resultBean.setCode(ResultBean.NO_LOGIN);
            response.setStatus(401);
            return resultBean;
        } else {
            resultBean.setMessage("已经登录");
            resultBean.setCode(ResultBean.HAS_LOGIN);
            return resultBean;
        }

    }


    /**
     * 前端传过来的密码  md5 （32） 位
     * @param req
     * @param response
     * @return
     */
    @RequestMapping(value = "sys/login",method = RequestMethod.POST)
    public ResultBean<String> login(HttpServletRequest req, HttpServletResponse response){

        ResultBean resultBean=new ResultBean();

        resultBean.setCode(ResultBean.FAIL);
        String exceptionClassName = (String)req.getAttribute("shiroLoginFailure");

        String message="";
        if(UnknownAccountException.class.getName().equals(exceptionClassName)) {
            message = "账号/密码错误";
        } else if(IncorrectCredentialsException.class.getName().equals(exceptionClassName)) {
            message ="账号/密码错误";
        }
        else if(LockedAccountException.class.getName().equals(exceptionClassName)) {
            message = "账号锁定";
        }
        else if(ExcessiveAttemptsException.class.getName().equals(exceptionClassName)) {
            message = "你输错的次数过多" ;
        }
        else if(AuthenticationException.class.getName().equals(exceptionClassName)) {
            message = "账号/密码错误" ;
        }
        else {
            message="已经登录";
            resultBean.setCode(ResultBean.SUCCESS);

        }
        resultBean.setMessage(message);

        return resultBean;
    }

    @RequestMapping(value = "sys/index")
    public ResultBean<SysUser> success(HttpServletRequest req){

        Subject currentUser = SecurityUtils.getSubject();
        SysUser user=this.sysUserService.findUserByName(currentUser.getPrincipal().toString());
        ResultBean<SysUser> resultBean=new ResultBean<>(user);


        return resultBean;

    }

    @RequestMapping(value = "logout",method=RequestMethod.POST)
    public ResultBean logout(HttpServletResponse response) throws Exception{
        SecurityUtils.getSubject().logout();
        return new ResultBean();

    }
}
