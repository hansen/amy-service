package door.smartlift.top.controller;

import door.smartlift.top.common.utils.StringUtil;
import door.smartlift.top.common.utils.WxSession;
import door.smartlift.top.domain.*;
import door.smartlift.top.exception.MyDefineException;
import door.smartlift.top.resultBean.ResultBean;
import door.smartlift.top.service.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Admin
 * @date 2018/4/1
 */
@Slf4j
@RestController
@RequestMapping("static")
public class TestController {

    @Autowired
    private IotGoodsService iotGoodsService;

    @Autowired
    private WxLoginService wxLoginService;

    @Autowired
    private WxUserService wxUserService;

    @Autowired
    private IotOrderService iotOrderService;

    @Autowired
    private GaAlarmService gaAlarmService;

    @Autowired
    private DtTeamService dtTeamService;

    @RequestMapping(value = "/goods", method = RequestMethod.GET, headers = "Accept=application/json")
    public ResultBean<Map> goods(@RequestParam String deviceNum) {

        log.info("===in  time===");
        Map data = this.iotGoodsService.selectByDeviceNum(deviceNum);
        log.info("===out  time===");
        return new ResultBean<>(data);
    }


    @RequestMapping(value = "/start", method = RequestMethod.GET, headers = "Accept=application/json")
    public ResultBean<Map> start(@RequestParam String device, String pulse, String num) {
        int iPulse = Integer.valueOf(pulse);
        int iNum = Integer.valueOf(num);

        Calendar cale = Calendar.getInstance();
        int month = cale.get(Calendar.MONTH) + 1;
        int day = cale.get(Calendar.DATE);
        int hour = cale.get(Calendar.HOUR_OF_DAY);
        int sum = month + day + hour;

        ResultBean rb = new ResultBean();


        if (iNum != sum) {
            rb.setCode(5);
            rb.setMessage("请不要乱试");
        } else {
            IotDevice iotDevice = this.iotGoodsService.getDeviceByDeviceNum(device);
            if (iotDevice != null) {

                String result=ChairCmd.sendCmd(iotDevice.getDeviceSn(), 50, 250, iPulse, "byhand");


                Map data=new HashMap();
                data.put("device", iotDevice);
                data.put("cmd",result);
                rb.setData(data);

            } else {
                rb.setCode(4);
                rb.setMessage("未找到设备编号");

            }

        }

        return rb;

    }

    @RequestMapping(value = "/bbb", method = RequestMethod.POST, headers = "Accept=application/json")
    public ResultBean<Integer> saveUser(@RequestBody WxUser wxUser) {


        int count = this.wxUserService.saveWxUser(wxUser);
        return new ResultBean<>(count);
    }

    @RequestMapping(value = "/unifiedOrder", method = RequestMethod.POST, headers = "Accept=application/json")
    public ResultBean unifiedOrder(@RequestBody IotGoods iotGoods) throws Exception {


        Map data = this.iotOrderService.doUnifiedOrder(iotGoods);
        return new ResultBean<>(data);
    }

    /**
     * 获取所有团队信息
     * @return
     */
    @RequestMapping(value = "dtpx/teams", method = RequestMethod.GET, headers = "Accept=application/json")
       public ResultBean<Map> teams() {

        log.info("===in  time===");
        List<DtTeam> list = this.dtTeamService.selectAll();
        log.info("===out  time===");

        Map data=new HashMap();
        data.put("list", list);
        return new ResultBean<>(data);
    }



    @RequestMapping(value = "dtpx/addTeam", method = RequestMethod.POST, headers = "Accept=application/json")
         public ResultBean addTeam(@RequestBody DtTeam dtTeam) throws Exception {

        log.info("===in  addTeam===");
        int result= this.dtTeamService.addDtTeam(dtTeam);

        ResultBean rb = new ResultBean();

        rb.setCode(0);
        rb.setMessage("ok");
        return rb;

//        org.springframework.dao.DuplicateKeyException:

//        {"teamName":"asdfasfd","teamPerson":"韩剑","loginSn":"1001","gameScore":-1,"teamBak":null,"status":null,"startTime":null,"endTime":null}

    }


    @RequestMapping(value = "dtpx/modifyTeam", method = RequestMethod.POST, headers = "Accept=application/json")
       public ResultBean modifyTeam(@RequestBody DtTeam dtTeam) throws Exception {

        log.info("===in  modifyTeam===");
        int result= this.dtTeamService.updateTeam(dtTeam);

        ResultBean rb = new ResultBean();

        if(result>0){
            rb.setCode(0);
            rb.setMessage("ok");
        }else{
            rb.setCode(1);
            rb.setMessage("update failed");
        }

        return rb;

//        org.springframework.dao.DuplicateKeyException:

//       {"id":4,"teamName":"asdfasfd","teamPerson":"韩剑6","loginSn":"1006","gameScore":-1,"teamBak":null,"status":null,"startTime":null,"endTime":null}
    }


    @RequestMapping(value = "dtpx/delTeam", method = RequestMethod.GET, headers = "Accept=application/json")
    public ResultBean delTeam(@RequestParam  Integer id) throws Exception {

        log.info("===in  delTeam===");
        int result= this.dtTeamService.deleteTeam(id);

        ResultBean rb = new ResultBean();

        if(result>0){
            rb.setCode(0);
            rb.setMessage("ok");
        }else{
            rb.setCode(1);
            rb.setMessage("del failed");
        }

        return rb;


//        Content-Type  header
//        org.springframework.dao.DuplicateKeyException:

//       {"id":4,"teamName":"asdfasfd","teamPerson":"韩剑6","loginSn":"1006","gameScore":-1,"teamBak":null,"status":null,"startTime":null,"endTime":null}
    }




    @RequestMapping(value = "dtpx/startGame", method = RequestMethod.GET, headers = "Accept=application/json")
    public ResultBean<Map> startGame() {

        log.info("===in  startGame===");
        int res = this.dtTeamService.startGame();
        ResultBean rb = new ResultBean();

        return rb;
    }


    @RequestMapping(value = "dtpx/endGame", method = RequestMethod.GET, headers = "Accept=application/json")
    public ResultBean<Map> endGame() {
        log.info("===in  startGame===");
        int res = this.dtTeamService.endGame();
        ResultBean rb = new ResultBean();
        return rb;
    }

    @RequestMapping(value = "dtpx/loginGame", method = RequestMethod.GET, headers = "Accept=application/json")
    public ResultBean<Map> loginGame(@RequestParam  String loginSn) {
        log.info("===in  loginGame===");
        ResultBean rb =this.dtTeamService.loginGame(loginSn);
        return rb;
    }



//    [{"loginSn":"1001","gameNo":"1","dockName":"扣分项1","dockId":"a001","dockScore":3,"createTime":null},{"loginSn":"1001","gameNo":"1","dockName":"扣分项2","dockId":"a003","dockScore":5,"createTime":null}]
    @RequestMapping(value = "dtpx/logoutGame", method = RequestMethod.POST, headers = "Accept=application/json")
    public ResultBean<Map> logoutGame(@RequestParam  String loginSn,@RequestParam  Integer score,@RequestParam  Integer durtion,@RequestBody List<DtRecord> list) {
        log.info("===in  logoutGame===");
        return this.dtTeamService.logoutGame(loginSn, list, score,durtion);
    }

    @RequestMapping(value = "dtpx/resetGame", method = RequestMethod.GET, headers = "Accept=application/json")
    public ResultBean<Map> resetGame(@RequestParam  String loginSn) {
        log.info("===in  resetGame===");
        ResultBean rb = this.dtTeamService.resetGame(loginSn);
        return rb;
    }


    @RequestMapping(value = "dtpx/getGameResult", method = RequestMethod.GET, headers = "Accept=application/json")
    public ResultBean<Map> getGameResult(@RequestParam  String loginSn) {
        log.info("===in  getGameResult===");
        List<DtRecord> list = this.dtTeamService.getGameResult(loginSn);


        Map data=new HashMap();
        data.put("res",list);

        return new ResultBean<>(data);

//        {"message":"success","code":0,"data":{"res":[{"id":1,"loginSn":"1001","gameNo":"1","dockName":"扣分项1","dockId":"a001","dockScore":3,"createTime":null},{"id":2,"loginSn":"1001","gameNo":"1","dockName":"扣分项2","dockId":"a003","dockScore":5,"createTime":null}]}}
    }


    /**
     * 获取基站的人数
     * @param stationNo
     * @param startTime
     * @param endTime
     * @return
     */
    @RequestMapping(value = "/station/person", method = RequestMethod.GET, headers = "Accept=application/json")
    public ResultBean<Map> stationPerson(@RequestParam String stationNo,@RequestParam String startTime,@RequestParam String endTime) {
        log.info("===in  stationPerson===");

        ResultBean rb = new ResultBean();

        rb.setCode(0);
        rb.setMessage("ok");

        Map data=new HashMap();
        data.put("person", 36);
        return new ResultBean<>(data);
    }


    @RequestMapping(value = "/alarm/face", method = RequestMethod.GET, headers = "Accept=application/json")
    public ResultBean<Map> getAlarmByTime(@RequestParam String startTime,@RequestParam String endTime) {
        log.info("===in  getAlarmByTime===");

//        ResultBean rb = new ResultBean();
//        rb.setCode(0);
//        rb.setMessage("ok");

        List<GaAlarm> list= gaAlarmService.selectByTime(startTime);


        Map data=new HashMap();
        data.put("alarms",list);
        return new ResultBean<>(data);
    }
}
