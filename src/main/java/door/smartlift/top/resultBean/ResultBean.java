package door.smartlift.top.resultBean;

import lombok.Data;

import java.io.Serializable;

/**
 *
 * @author Admin
 * @date 2018/1/22
 */
@Data
public class ResultBean<T> implements Serializable {

    private static final long serialVersionUID=1L;

    public static final int SUCCESS=0;

    public static final int FAIL=1;

    public static final int NO_PREMISSION=2;

    public static final int NO_LOGIN=5;

    public static final int HAS_LOGIN=4003;

    private String message="success";

    private int code=SUCCESS;

    private T data;

    public ResultBean(){
        super();
    }

    public ResultBean(T data){
        super();
        this.data=data;
    }

    public ResultBean(Throwable t){
        super();
        this.message=t.getMessage();
        this.code=FAIL;
    }
}
