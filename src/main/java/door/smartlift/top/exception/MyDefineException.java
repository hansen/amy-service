package door.smartlift.top.exception;

import org.springframework.dao.DataIntegrityViolationException;

/**
 *
 * @author Admin
 * @date 2018/3/29
 */
public class MyDefineException extends DataIntegrityViolationException {

    public MyDefineException(String message){

        super(message);
    }
}
