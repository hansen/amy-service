package door.smartlift.top.service;

import door.smartlift.top.domain.SysUser;

/**
 *
 * @author Admin
 * @date 2018/4/1
 */
public interface SysUserService {

    /**
     * 根据用户名查找用户
     * @param username
     * @return
     */
    SysUser findUserByName(String username);

    /**
     * 创建用户
     * @param sysUser
     * @return
     */
    int createUser(SysUser sysUser);
}
