package door.smartlift.top.service;

import door.smartlift.top.domain.DtTeam;
import door.smartlift.top.domain.GaAlarm;

import java.util.List;

/**
 *
 * @author Admin
 * @date 2018/4/1
 */
public interface GaAlarmService {

    List<GaAlarm> selectByTime(String startTime);

}
