package door.smartlift.top.service;

import door.smartlift.top.domain.WxUser;

/**
 *小程序登陆
 * @author Admin
 * @date 2018/3/29
 */
public interface WxLoginService {

    /**
     * code获取会话信息
     * @param code
     * @return
     */
    String wxLogin(String code);

    /**
     * 根据sessionId获取用户信息
     * @param sessionId
     * @return
     */
    WxUser findWxUserBySessionId(String sessionId);


}
