package door.smartlift.top.service;

import door.smartlift.top.domain.WxUser;
import door.smartlift.top.domain.base.BaseDateMembers;
import door.smartlift.top.domain.base.PageCommand;

import java.util.List;

/**
 *
 * @author Admin
 * @date 2018/4/6
 */
public interface WxUserService {

    /**
     * 保存微信用户信息
     * @param wxUser
     * @return
     */
    int saveWxUser(WxUser wxUser);

    /**
     * 查询微信用户列表，分页
     * @return
     */
    BaseDateMembers selectWxUserList(PageCommand pageCommand);
}
