package door.smartlift.top.service;

import door.smartlift.top.domain.SysConstant;

import java.util.List;

/**
 *
 * @author Admin
 * @date 2018/4/1
 */
public interface SysConstantService {

    /**
     * 查询系统常量
     * @return
     */
    List<SysConstant> selectAllConstant();

    /**
     * 根据常量类型查询
     * @param code
     * @return
     */
    List<SysConstant> selectByConstantCode(String code);
}
