package door.smartlift.top.service;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;

import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONObject;

import org.apache.commons.codec.binary.Base64;

import com.alibaba.fastjson.JSON;

@Slf4j
public class SendLauncherChair {

    private static final String URL = "url";
    private static final String APP_KEY = "pfHsdtg2rASFK84m";
    private static final String SECRET_KEY = "ooo3Wx9OkLZkwLYaNzgBrh3wTwT1yO0mjuBRAq0PmpAxmG9BFvtQinm1Z7LtELY4";

    /**
     * 构造Basic Auth认证头信息
     *
     * @return
     */
    public static String getHeader() {
        String auth = APP_KEY + ":" + SECRET_KEY;
        byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("US-ASCII")));
        String authHeader = "Basic " + new String(encodedAuth);
        return authHeader;
    }

    /**
     * 启动按摩椅
     * *
     */
    public static final String ADD_URL = "http://api.openluat.com/mafu/event/launch_pulse_signal_deivce";

    /**
     * **
     *
     * @param imei  硬件编号
     * @param high  高电平持续时间 50
     * @param low   低电平持续时间 250
     * @param pulse 脉冲数
     */
    public static void launcherChairs2(String imei, int high, int low, int pulse, String orderCode) {
        try {
            //创建连接


            log.info("=====in  launcherChairs ===== ");
            URL url = new URL(ADD_URL);
            HttpURLConnection connection = (HttpURLConnection) url
                    .openConnection();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestMethod("POST");
            connection.setUseCaches(false);
            connection.setInstanceFollowRedirects(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Authorization", getHeader());
            connection.connect();

            log.info("=====in  launcherChairs ==1111= ");
            //POST请求
            DataOutputStream out = new DataOutputStream(
                    connection.getOutputStream());
            JSONObject obj = new JSONObject();
            obj.put("imei", imei);
            obj.put("high", high);
            obj.put("low", low);
            obj.put("pulse", pulse);
            out.writeBytes(obj.toString());
            out.flush();
            out.close();
            log.info("=====in  launcherChairs ==2222= ");
            //读取响应
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String lines;
            StringBuffer sb = new StringBuffer("");
            while ((lines = reader.readLine()) != null) {
                lines = new String(lines.getBytes(), "utf-8");
                sb.append(lines);
            }
            log.info("=====in  launcherChairs ==33333= ");
            System.out.println(sb);
            log.info("=====in   ===== ");
            connection.disconnect();
            log.info("设备imei：" + imei + "--》连接成功，设备返回信息：" + sb.toString());
        } catch (Exception e) {
            e.printStackTrace();
            log.info("设备imei:" + imei + "--》连接失败hh:" + e.getMessage(), e.getMessage());
        }
    }

    /**
     * 获取硬件状态
     */
    public static final String ADD_STATUS = "http://api.openluat.com/mafu/event/device/status";

    public static String getImeiStatus2(String imei) {
        try {
            //创建连接
            URL url = new URL(ADD_STATUS);
            HttpURLConnection connection = (HttpURLConnection) url
                    .openConnection();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestMethod("POST");
            connection.setUseCaches(false);
            connection.setInstanceFollowRedirects(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Authorization", getHeader());
            connection.connect();
            //POST请求
            DataOutputStream out = new DataOutputStream(
                    connection.getOutputStream());
            JSONObject obj = new JSONObject();
            obj.put("imei", imei);
            out.writeBytes(obj.toString());
            out.flush();
            out.close();
            //读取响应
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String lines;
            StringBuffer sb = new StringBuffer("");
            while ((lines = reader.readLine()) != null) {
                lines = new String(lines.getBytes(), "utf-8");
                sb.append(lines);
            }
            System.out.println("sb.toString()" + sb.toString());
            connection.disconnect();
            com.alibaba.fastjson.JSONObject job = JSON.parseObject(sb.toString());
            com.alibaba.fastjson.JSONObject job_data = (com.alibaba.fastjson.JSONObject) job.get("data");
            String status = job_data.getString("status");
            System.out.println(status);
            if (status.equals("0")) {
                return "离线";
            }
            if (status.equals("1")) {
                return "在线";
            }
            if (status.equals("4")) {
                return "关机";
            }
            return "异常1";
        } catch (Exception e) {
            e.printStackTrace();
            log.info("设备imei:" + imei + "--》获取状态失败" + e.getMessage(), e.getMessage());
            return "异常2";
        }
    }

    public static void main(String[] args) {

        SendLauncherChair.launcherChairs2("868575021549755", 50, 250, 2, "aaa");
    }
}