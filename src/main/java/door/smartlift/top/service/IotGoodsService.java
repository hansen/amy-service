package door.smartlift.top.service;

import door.smartlift.top.domain.IotDevice;
import door.smartlift.top.domain.IotGoods;

import java.util.List;
import java.util.Map;

/**
 *
 * @author Admin
 * @date 2018/4/1
 */
public interface IotGoodsService {

    /**
     * 新增价格方案
     * @param iotGoods
     * @return
     */
    int addGoods(IotGoods iotGoods);


    /**
     * 根据商户id查询价格方案
     * @param bid
     * @return
     */
    Map selectAllGoodsByBid(Integer bid);

    /**
     * 根据id修改价格方案
     * @param iotGoods
     * @return
     */
    int updateGoodsById(IotGoods iotGoods);

    /**
     * 根据id删除价格方案
     * @param goodsId
     * @return
     */
    int deleteGoodsById(Integer goodsId);

    /**
     * 根据设备编号查询价格方案
     * @param deviceNum
     * @return
     */
    Map selectByDeviceNum(String deviceNum);

    /**
     * 根据设备编号查询设备基本信息
     * @param deviceNum
     * @return
     */
    IotDevice getDeviceByDeviceNum(String deviceNum);
}
