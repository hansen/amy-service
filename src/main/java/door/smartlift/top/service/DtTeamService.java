package door.smartlift.top.service;

import door.smartlift.top.domain.*;
import door.smartlift.top.resultBean.ResultBean;

import java.util.List;
import java.util.Map;

/**
 *
 * @author Admin
 * @date 2018/4/1
 */
public interface DtTeamService {


    int addDtTeam(DtTeam dtTeam);
    int updateTeam(DtTeam dtTeam);
    int deleteTeam(Integer id);
    DtTeam findTeamById(Integer id);

    List<DtTeam> selectAll();

    List<DtTeam> getTeamOrderLogin();
    List<DtTeam> getTeamOrderScore();
    List<DtTeam> getTeamOrderDuration();

    int startGame();
    int endGame();
    DtApp getGame();


    ResultBean loginGame(String loginSn);
    ResultBean logoutGame(String loginSn,List<DtRecord> recList,int score,int durtion);
    ResultBean resetGame(String loginSn);
    List<DtRecord> getGameResult(String loginSn);

}
