package door.smartlift.top.service;

import com.github.binarywang.wxpay.bean.order.WxPayMpOrderResult;
import door.smartlift.top.domain.IotGoods;
import door.smartlift.top.domain.base.BaseDateMembers;
import door.smartlift.top.domain.base.PageCommand;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Admin
 * @date 2018/4/2
 */
public interface IotOrderService {

    /**
     * 下单
     * @param goods
     * @return
     */
    Map doUnifiedOrder(IotGoods goods) throws Exception;

    /**
     * 支付回调
     * @param request
     */
    String conformWxPay(HttpServletRequest request) throws Exception;

    /**
     * 交易记录查询
     * @param pageCommand
     * @param payStatus
     * @param deviceNum
     * @param wxOrderCode
     * @return
     */
    BaseDateMembers transactionRecord(PageCommand pageCommand, String payStatus, String deviceNum, String wxOrderCode);
}
