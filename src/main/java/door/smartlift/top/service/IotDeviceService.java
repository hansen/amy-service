package door.smartlift.top.service;

import door.smartlift.top.domain.IotDevice;

import java.util.List;

/**
 *
 * @author Admin
 * @date 2018/4/1
 */
public interface IotDeviceService {

    /**
     * 新增设备
     * @param iotDevice
     * @return
     */
    int createDevice(IotDevice iotDevice);

    /**
     * 查询设备列表
     * @param bid
     * @param deviceType
     * @return
     */
    List<IotDevice> selectDeviceByExample(Integer bid,String deviceType);

    /**
     * 分配设备
     * @param device
     * @return
     */
    int alloteDevice(IotDevice device);

    /**
     * 更新设备
     * @param iotDevice
     * @return
     */
    int updateDevice(IotDevice iotDevice);

    /**
     * 根据id查询设备
     * @param id
     * @return
     */
    IotDevice selectDeviceById(Integer id);
}
