package door.smartlift.top.service.impl;

import door.smartlift.top.common.utils.CollectionUtil;
import door.smartlift.top.common.utils.CurrentUser;
import door.smartlift.top.dao.IotBusinessMapper;
import door.smartlift.top.dao.IotDeviceMapper;
import door.smartlift.top.domain.IotBusiness;
import door.smartlift.top.domain.IotDevice;
import door.smartlift.top.exception.MyDefineException;
import door.smartlift.top.service.ChairCmd;
import door.smartlift.top.service.IotDeviceService;
import door.smartlift.top.service.SendLauncherChair;
import door.smartlift.top.service.SysUserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.map.HashedMap;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Admin
 * @date 2018/4/1
 */
@Slf4j
@Service("iotDeviceService")
public class IotDeviceServiceImpl implements IotDeviceService {

    @Autowired
    private IotDeviceMapper iotDeviceMapper;

    @Autowired
    private IotBusinessMapper iotBusinessMapper;

    @Autowired
    private SysUserService sysUserService;

    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public int createDevice(IotDevice iotDevice) {

        Map params=new HashMap();
        params.put("deviceNum",iotDevice.getDeviceNum());
        params.put("deviceSn",iotDevice.getDeviceSn());
        params.put("mobile",iotDevice.getMobile());

        List<IotDevice> deviceList=this.iotDeviceMapper.selectSameDevice(params);

        if (CollectionUtil.isNotEmpty(deviceList)){

            throw new MyDefineException("请检查设备编号，imei号，sim卡号是否在系统中已存在");
        }
        //平台id
        Integer parentId= this.sysUserService.findUserByName(SecurityUtils.getSubject().getPrincipal().toString()).getBelong();
        iotDevice.setParentId(parentId);

        //默认为平台设备
        if (iotDevice.getBid()==null){

            iotDevice.setBid(parentId);
        }


        return this.iotDeviceMapper.insertSelective(iotDevice);
    }

    @Override
    public List<IotDevice> selectDeviceByExample(Integer bid, String deviceType) {


        Map params=new HashedMap();

        params.put("deviceType",deviceType);

        if (bid !=null){
            params.put("bid",bid);
        }else {

            Integer belongId=this.sysUserService.findUserByName(SecurityUtils.getSubject().getPrincipal().toString()).getBelong();

            IotBusiness iotBusiness=this.iotBusinessMapper.selectByPrimaryKey(belongId);

            if (iotBusiness.getParentId()==null){

                params.put("parentId",iotBusiness.getBid());
            }else {
                params.put("bid",iotBusiness.getBid());
            }
        }

        List<IotDevice> deviceList=this.iotDeviceMapper.selectDeviceByExample(params);

        if (CollectionUtil.isNotEmpty(deviceList)){

            //获取设备状态
            for (IotDevice device:deviceList){

                String imei=device.getDeviceSn();

//                String deviceStatus= SendLauncherChair.getImeiStatus(imei);
                String deviceStatus= ChairCmd.getImeiStatus(imei);

                device.setState(deviceStatus);
            }
        }

        return deviceList;
    }

    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public int alloteDevice(IotDevice device) {

        return this.iotDeviceMapper.updateByPrimaryKeySelective(device);
    }

    @Override
    @Transactional
    public int updateDevice(IotDevice iotDevice) {

        Map params=new HashMap();
        params.put("deviceNum",iotDevice.getDeviceNum());
        params.put("deviceSn",iotDevice.getDeviceSn());
        params.put("mobile",iotDevice.getMobile());
        params.put("id",iotDevice.getId());

        List<IotDevice> deviceList=this.iotDeviceMapper.selectSameDevice(params);

        if (CollectionUtil.isNotEmpty(deviceList)){

            throw new MyDefineException("请检查设备编号，imei号，sim卡号是否在系统中已存在");
        }

        return this.iotDeviceMapper.updateByPrimaryKeySelective(iotDevice);
    }


    @Override
    public IotDevice selectDeviceById(Integer id) {

        return this.iotDeviceMapper.selectByPrimaryKey(id);
    }
}
