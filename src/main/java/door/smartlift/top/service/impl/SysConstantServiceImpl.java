package door.smartlift.top.service.impl;

import door.smartlift.top.dao.SysConstantMapper;
import door.smartlift.top.domain.SysConstant;
import door.smartlift.top.domain.SysConstantExample;
import door.smartlift.top.service.SysConstantService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 * @author Admin
 * @date 2018/4/1
 */
@Slf4j
@Service("sysConstantService")
public class SysConstantServiceImpl implements SysConstantService {

    @Autowired
    private SysConstantMapper sysConstantMapper;

    @Override
    public List<SysConstant> selectAllConstant() {

        return this.sysConstantMapper.selectAllConstant();
    }


    @Override
    public List<SysConstant> selectByConstantCode(String code){

        SysConstantExample sysConstantExample=new SysConstantExample();

        sysConstantExample.createCriteria().andCodeEqualTo(code);

        return this.sysConstantMapper.selectByExample(sysConstantExample);
    }
}
