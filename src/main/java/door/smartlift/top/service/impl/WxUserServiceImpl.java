package door.smartlift.top.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import door.smartlift.top.common.utils.WxSession;
import door.smartlift.top.dao.WxUserMapper;
import door.smartlift.top.domain.WxUser;
import door.smartlift.top.domain.base.BaseDateMembers;
import door.smartlift.top.domain.base.PageCommand;
import door.smartlift.top.service.WxUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 *
 * @author Admin
 * @date 2018/4/6
 */
@Service("wxUserService")
public class WxUserServiceImpl implements WxUserService {

    @Autowired
    private WxUserMapper wxUserMapper;


    @Override
    @Transactional
    public int saveWxUser(WxUser wxUser) {

        String sessionId=WxSession.getSessionId();
        wxUser.setSessionKey(sessionId);
        return this.wxUserMapper.updateWxUserBySession(wxUser);
    }

    @Override
    public BaseDateMembers selectWxUserList(PageCommand pageCommand) {

        Page<WxUser> page=PageHelper.startPage(pageCommand.getPageIndex(),pageCommand.getPageSize());

        List<WxUser> userList=this.wxUserMapper.selectAllWxUser();

        pageCommand.setTotalPages(page.getPages());
        pageCommand.setPageTotal(page.getTotal());


        return new BaseDateMembers(pageCommand,userList);
    }
}
