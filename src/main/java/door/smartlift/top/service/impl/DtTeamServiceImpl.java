package door.smartlift.top.service.impl;

import door.smartlift.top.common.utils.CollectionUtil;
import door.smartlift.top.dao.*;
import door.smartlift.top.domain.*;
import door.smartlift.top.exception.MyDefineException;
import door.smartlift.top.resultBean.ResultBean;
import door.smartlift.top.service.DtTeamService;
import door.smartlift.top.service.IotBusinessService;
import door.smartlift.top.service.SysUserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 *
 * @author Admin
 * @date 2018/4/1
 */
@Slf4j
@Service("dtTeamService")
public class DtTeamServiceImpl implements DtTeamService {

    @Autowired
    private DtTeamMapper dtTeamMapper;

    @Autowired
    private DtAppMapper dtAppMapper;

    @Autowired
    private DtRecordMapper dtRecordMapper;


    @Override
    public int addDtTeam(DtTeam dtTeam) {

        int count=this.dtTeamMapper.insertSelective(dtTeam);

        return count;
    }

    @Override
    public int updateTeam(DtTeam dtTeam) {
        int result=dtTeamMapper.updateByPrimaryKeySelective(dtTeam);
        return result;
    }

    @Override
    public int deleteTeam(Integer id) {

        int result=dtTeamMapper.deleteByPrimaryKey(id);
        return result;

    }

    @Override
    public DtTeam findTeamById(Integer id) {
        DtTeam dtTeam=dtTeamMapper.selectByPrimaryKey(id);
        return dtTeam;
    }

    @Override
    public List<DtTeam> selectAll() {

        DtTeamExample example=new DtTeamExample();
        example.createCriteria();

        List<DtTeam> dtTeamList=this.dtTeamMapper.selectByExample(example);

        return dtTeamList;

    }

    @Override
    public List<DtTeam> getTeamOrderLogin() {
        DtTeamExample example=new DtTeamExample();
        example.createCriteria();
        example.setOrderByClause("login_sn");

        List<DtTeam> dtTeamList=this.dtTeamMapper.selectByExample(example);

        return dtTeamList;
    }

    @Override
    public List<DtTeam> getTeamOrderScore() {
        DtTeamExample example=new DtTeamExample();
        example.createCriteria();
        example.setOrderByClause("game_score desc,login_sn");

        List<DtTeam> dtTeamList=this.dtTeamMapper.selectByExample(example);

        return dtTeamList;
    }

    @Override
    public List<DtTeam> getTeamOrderDuration() {
        DtTeamExample example=new DtTeamExample();
        example.createCriteria();
        example.setOrderByClause("durtion,login_sn ");

        List<DtTeam> dtTeamList=this.dtTeamMapper.selectByExample(example);

        return dtTeamList;
    }

    @Override
    public int startGame() {
        DtApp dtApp=dtAppMapper.selectByPrimaryKey(1);

        dtApp.setIsOpen(1);
        dtAppMapper.updateByPrimaryKey(dtApp);
        return 0;

    }

    @Override
    public int endGame() {
        DtApp dtApp=dtAppMapper.selectByPrimaryKey(1);
        dtApp.setIsOpen(0);
        dtAppMapper.updateByPrimaryKey(dtApp);
        return 0;
    }

    @Override
    public DtApp getGame() {
        DtApp dtApp=dtAppMapper.selectByPrimaryKey(1);
        return dtApp;
    }

    @Override
    public ResultBean loginGame(String loginSn) {
        ResultBean rb=new ResultBean();


        if(!gameIsOpen()){
            rb.setCode(10);
            rb.setMessage("比赛还未开始请等待");
            return rb;
        }

        //-----------

        DtTeam  dtTeam=getTeamByLoginsn(loginSn);
        if(dtTeam ==null){
            rb.setCode(1);
            rb.setMessage("登录编号未找到");
        }else{
            if(dtTeam.getStatus()>0){
                rb.setCode(2);
                rb.setMessage("已经登录或已结束考试");
            }else{
                dtTeam.setStatus(1);
                dtTeamMapper.updateByPrimaryKeySelective(dtTeam);

                rb.setCode(0);
                rb.setMessage("登录成功，请比赛");
            }
        }

        return rb;
    }

    @Override
    public ResultBean logoutGame(String loginSn,List<DtRecord> recList, int score,int durtion) {
        ResultBean rb=new ResultBean();
        if(!gameIsOpen()){
            rb.setCode(10);
            rb.setMessage("比赛已结束，不能提交成绩");
            return rb;
        }

        DtTeam  dtTeam=getTeamByLoginsn(loginSn);
        if(dtTeam ==null){
            rb.setCode(1);
            rb.setMessage("登录编号未找到");
        }else{

//            dtTeam.setEndTime();

            //xun huan  rec  insert


            for(int i=0;i<recList.size();i++){
                DtRecord temp=(DtRecord)recList.get(i);
                temp.setLoginSn(loginSn);
                dtRecordMapper.insert(temp);
                score=score-temp.getDockScore();
            }
            dtTeam.setStatus(2);//结束
            dtTeam.setGameScore(score);
            dtTeam.setDurtion(durtion);
            dtTeamMapper.updateByPrimaryKeySelective(dtTeam);

            rb.setCode(0);
            rb.setMessage("ok");
        }

        return rb;
    }


    /**
     * 更新状态 和分数 ，删除之前的记录
     * @param loginSn
     * @return
     */
    @Override
    public ResultBean resetGame(String loginSn) {

        DtTeam  dtTeam=getTeamByLoginsn(loginSn);
        List<DtRecord> recList=getGameResult(loginSn);
        for(int i=0;i<recList.size();i++){
            DtRecord temp=(DtRecord)recList.get(i);

            dtRecordMapper.deleteByPrimaryKey(temp.getId());
        }

        dtTeam.setStatus(0);
        dtTeam.setGameScore(-1);
        dtTeam.setDurtion(0);
        int res=dtTeamMapper.updateByPrimaryKeySelective(dtTeam);

        ResultBean rb=new ResultBean();

        rb.setCode(0);
        rb.setMessage("ok");
        return rb;
    }

    @Override
    public List<DtRecord> getGameResult(String loginSn) {

        DtRecordExample example=new DtRecordExample();
        example.createCriteria().andLoginSnEqualTo(loginSn);
        List<DtRecord> list=dtRecordMapper.selectByExample(example);
        return list;
    }



    public boolean gameIsOpen(){
        DtApp dtApp=dtAppMapper.selectByPrimaryKey(1);

        if(dtApp.getIsOpen()==0){
            return false;
        }else{
            return true;
        }
    }


    public DtTeam getTeamByLoginsn(String loginsn){
        DtTeamExample example=new DtTeamExample();
        example.createCriteria().andLoginSnEqualTo(loginsn);
        List<DtTeam> list= dtTeamMapper.selectByExample(example);

        log.info("===="+loginsn  +" list:"+list.size());

        if(list.size()>=1){
            DtTeam dtTeam=(DtTeam) list.get(0);
            return dtTeam;
        }else{
            return null;
        }
    }
}
