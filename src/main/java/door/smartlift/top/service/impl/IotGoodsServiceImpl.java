package door.smartlift.top.service.impl;

import door.smartlift.top.common.utils.CollectionUtil;
import door.smartlift.top.dao.IotDeviceMapper;
import door.smartlift.top.dao.IotGoodsMapper;
import door.smartlift.top.domain.*;
import door.smartlift.top.exception.MyDefineException;
import door.smartlift.top.service.ChairCmd;
import door.smartlift.top.service.IotGoodsService;
import door.smartlift.top.service.SendLauncherChair;
import door.smartlift.top.service.SysConstantService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Admin
 * @date 2018/4/1
 */
@Slf4j
@Service("iotGoodsService")
public class IotGoodsServiceImpl implements IotGoodsService {

    @Autowired
    private IotGoodsMapper iotGoodsMapper;

    @Autowired
    private IotDeviceMapper iotDeviceMapper;

    @Autowired
    private SysConstantService sysConstantService;


    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public int addGoods(IotGoods iotGoods) {

        return this.iotGoodsMapper.insertSelective(iotGoods);
    }

    @Override
    public Map selectAllGoodsByBid(Integer bid) {

        IotGoodsExample iotGoodsExample=new IotGoodsExample();

        iotGoodsExample.createCriteria().andBidEqualTo(bid);

        List<IotGoods> goodsList=this.iotGoodsMapper.selectByExample(iotGoodsExample);

        List<SysConstant> constants=this.sysConstantService.selectByConstantCode("DEIVICE_TYPE");

        if (CollectionUtil.isNotEmpty(constants)){

            Map data=new HashedMap();

            for (SysConstant constant:constants){

                List<IotGoods> iotGoodsList=new ArrayList<>();

                data.put(constant.getCodeValue(),iotGoodsList);

                if (CollectionUtil.isNotEmpty(goodsList)){

                    for (IotGoods goods:goodsList){

                        if (goods.getDtype().equals(constant.getCodeValue())){

                            iotGoodsList.add(goods);
                        }
                    }
            }
        }

            return data;
        }
        return null;
    }

    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public int updateGoodsById(IotGoods iotGoods) {

        return this.iotGoodsMapper.updateByPrimaryKeySelective(iotGoods);
    }

    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public int deleteGoodsById(Integer goodsId) {

        return this.iotGoodsMapper.deleteByPrimaryKey(goodsId);
    }


    @Override
    public Map selectByDeviceNum(String deviceNum) {

        IotDeviceExample iotDeviceExample=new IotDeviceExample();

        iotDeviceExample.createCriteria().andDeviceNumEqualTo(deviceNum);

        List<IotDevice> deviceList=this.iotDeviceMapper.selectByExample(iotDeviceExample);

        if (CollectionUtil.isEmpty(deviceList)){

            throw new MyDefineException("设备不存在");
        }

        IotDevice device=deviceList.get(0);

        log.info("===in selectByDeviceNum:"+device.getDeviceSn());

        //获取设备状态
//        String status=SendLauncherChair.getImeiStatus(device.getDeviceSn());
        String status= ChairCmd.getImeiStatus(device.getDeviceSn());

        IotGoodsExample iotGoodsExample=new IotGoodsExample();
        iotGoodsExample.setOrderByClause("duration");
        iotGoodsExample.createCriteria().andBidEqualTo(deviceList.get(0).getBid())
                .andDtypeEqualTo(device.getDeviceType());

        Map data=new HashMap();

        data.put("status",status);

        List<IotGoods> goodss=this.iotGoodsMapper.selectByExample(iotGoodsExample);
        data.put("goods",goodss);

        return data;
    }

    @Override
    public IotDevice getDeviceByDeviceNum(String deviceNum) {
        IotDeviceExample iotDeviceExample=new IotDeviceExample();
        iotDeviceExample.createCriteria().andDeviceNumEqualTo(deviceNum);
        List<IotDevice> deviceList=this.iotDeviceMapper.selectByExample(iotDeviceExample);
        if (CollectionUtil.isEmpty(deviceList)){
//            throw new MyDefineException("设备不存在");
            return null;
        }

        IotDevice device=deviceList.get(0);

        return device;
    }
}
