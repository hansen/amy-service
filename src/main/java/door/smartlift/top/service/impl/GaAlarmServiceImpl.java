package door.smartlift.top.service.impl;

import door.smartlift.top.dao.DtTeamMapper;
import door.smartlift.top.dao.GaAlarmMapper;
import door.smartlift.top.domain.DtTeam;
import door.smartlift.top.domain.DtTeamExample;
import door.smartlift.top.domain.GaAlarm;
import door.smartlift.top.domain.GaAlarmExample;
import door.smartlift.top.service.GaAlarmService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 * @author Admin
 * @date 2018/4/1
 */
@Slf4j
@Service("gaAlarmService")
public class GaAlarmServiceImpl implements GaAlarmService {

    @Autowired
    private GaAlarmMapper gaAlarmMapper;


    @Override
    public List<GaAlarm> selectByTime(String startTime) {
        GaAlarmExample example=new GaAlarmExample();
        example.createCriteria();
        List<GaAlarm> list=this.gaAlarmMapper.selectByExample(example);

        return list;


    }
}
