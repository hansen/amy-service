package door.smartlift.top.service.impl;

import door.smartlift.top.common.utils.CollectionUtil;
import door.smartlift.top.dao.IotBusinessMapper;
import door.smartlift.top.dao.SysUserMapper;
import door.smartlift.top.domain.IotBusiness;
import door.smartlift.top.domain.IotBusinessExample;
import door.smartlift.top.domain.SysUser;
import door.smartlift.top.exception.MyDefineException;
import door.smartlift.top.service.IotBusinessService;
import door.smartlift.top.service.SysUserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 *
 * @author Admin
 * @date 2018/4/1
 */
@Slf4j
@Service("iotBusinessService")
public class IotBusinessServiceImpl implements IotBusinessService {

    @Autowired
    private IotBusinessMapper iotBusinessMapper;

    @Autowired
    private SysUserService sysUserService;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public int createBusiness(IotBusiness iotBusiness) {

        iotBusiness.setParentId(this.sysUserService.findUserByName(SecurityUtils.getSubject().getPrincipal().toString()).getBelong());

        IotBusinessExample iotBusinessExample=new IotBusinessExample();

        iotBusinessExample.createCriteria().andPhoneEqualTo(iotBusiness.getPhone());

        List<IotBusiness> iotBusinesses= this.iotBusinessMapper.selectByExample(iotBusinessExample);

        if (CollectionUtil.isNotEmpty(iotBusinesses)){

            throw new MyDefineException("电话号码重复");
        }

        int count=this.iotBusinessMapper.insertSelective(iotBusiness);

        SysUser sysUser=new SysUser();

        sysUser.setPassword(iotBusiness.getPassword());
        sysUser.setPhone(iotBusiness.getPhone());
        sysUser.setUsername(iotBusiness.getPhone());
        sysUser.setBelong(iotBusiness.getBid());

        this.sysUserService.createUser(sysUser);

        return count;
    }


    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public int updateBusiness(IotBusiness iotBusiness) {

        IotBusinessExample iotBusinessExample=new IotBusinessExample();

        iotBusinessExample.createCriteria()
                .andPhoneEqualTo(iotBusiness.getPhone())
                .andBidNotEqualTo(iotBusiness.getBid());

        List<IotBusiness> iotBusinesses= this.iotBusinessMapper.selectByExample(iotBusinessExample);

        if (CollectionUtil.isNotEmpty(iotBusinesses)){

            throw new MyDefineException("电话号码重复");
        }

        IotBusiness old=this.iotBusinessMapper.selectByPrimaryKey(iotBusiness.getBid());

        int count=this.iotBusinessMapper.updateByPrimaryKeySelective(iotBusiness);


        SysUser oldUser=this.sysUserService.findUserByName(old.getPhone());

        SysUser sysUser=new SysUser();
        sysUser.setUsername(iotBusiness.getPhone());
        sysUser.setPhone(iotBusiness.getPhone());
        sysUser.setUid(oldUser.getUid());

        this.sysUserMapper.updateByPrimaryKeySelective(sysUser);
        return count;
    }

    @Override
    public List<IotBusiness> selectAllBusiness() {


        return this.iotBusinessMapper.selectAllBusiness();
    }
}
