package door.smartlift.top.service;

import door.smartlift.top.domain.IotBusiness;

import java.util.List;

/**
 *
 * @author Admin
 * @date 2018/4/1
 */
public interface IotBusinessService {

    /**
     * 创建商家
     * @param iotBusiness
     * @return
     */
    int createBusiness(IotBusiness iotBusiness);

    /**
     * 修改商家信息
     * @param iotBusiness
     * @return
     */
    int updateBusiness(IotBusiness iotBusiness);

    /**
     * 查询所有的商户
     * @return
     */
    List<IotBusiness> selectAllBusiness();
}
