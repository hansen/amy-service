package door.smartlift.top.aop;

import door.smartlift.top.resultBean.ResultBean;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 * @author Admin
 * @date 2018/1/23
 */
@RestControllerAdvice
@Slf4j
public class ControllerAop {

    @ExceptionHandler(RuntimeException.class)
    private ResultBean<?> handlerControllerMethod(Exception e){

        e.printStackTrace();
        ResultBean<?> result=new ResultBean<>();

        String exceptionName="door.smartlift.top.exception.MyDefineException";

        if (exceptionName.equals(e.getClass().getName())){
            result.setMessage(e.getMessage());
        }else {
            result.setMessage("操作失败");
        }

        result.setCode(ResultBean.FAIL);

        return result;
    }
}
