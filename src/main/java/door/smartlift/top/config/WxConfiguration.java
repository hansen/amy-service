package door.smartlift.top.config;

import com.github.binarywang.wxpay.config.WxPayConfig;
import com.github.binarywang.wxpay.service.WxPayService;
import com.github.binarywang.wxpay.service.impl.WxPayServiceImpl;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Admin on 2018/5/7.
 */
@Configuration
public class WxConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public WxPayConfig payConfig(){

        WxPayConfig payConfig = new WxPayConfig();
        payConfig.setAppId("wxdb635c18c3a7ae7f");
        payConfig.setMchId("1491308042");
        payConfig.setMchKey("6C5B77BC23A2BCAA5EE5AF7EB4938D5A");

        return payConfig;
    }

    @Bean
    @ConditionalOnMissingBean
    public WxPayService payService(WxPayConfig payConfig){

        WxPayService wxPayService = new WxPayServiceImpl();
        wxPayService.setConfig(payConfig);

        return wxPayService;

    }
}
