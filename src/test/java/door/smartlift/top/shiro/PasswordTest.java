package door.smartlift.top.shiro;

import door.smartlift.top.SmartLifeApplication;
import org.apache.shiro.crypto.RandomNumberGenerator;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by Admin on 2018/4/1.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = SmartLifeApplication.class)
public class PasswordTest {

    @Test
    public void testPassword(){

        RandomNumberGenerator randomNumberGenerator = new SecureRandomNumberGenerator();

        String salt=randomNumberGenerator.nextBytes().toHex();

        String newPassword = new SimpleHash(
                "md5",
                "55587a910882016321201e6ebbc9f595",
                ByteSource.Util.bytes(salt),
                1).toHex();

        System.out.println(salt);
        System.out.println(newPassword);

    }
}
